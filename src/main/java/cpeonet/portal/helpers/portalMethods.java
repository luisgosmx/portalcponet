/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cpeonet.portal.helpers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static cponet.portal.servlets.mainServlet.conttable;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.jsoup.nodes.Document;

/**
 *
 * @author golivares
 */
public class portalMethods {

    //company search match in elasticsearch
    //the function of this method is to query elastic to see if any json document contains the word and if it returns the complete document.
    public static ArrayList searchElasticSearch(ArrayList words) throws IOException {
        ArrayList ids = new ArrayList();
        int cont = 0;
        String resultado = "";
        for (int x = 0; x < words.size(); x++) {
            String word = words.get(x).toString();
            String id = null;
            String score = null;
            int icurrent = 0;
            int irowcount = 100;

            String searchFrase = word;
            String scollection = "textnews";

            //query on elasticsearc
            HttpClient httpClient = HttpClientBuilder.create().build();
            String query = "{\n"
                    + "  \"from\" : " + icurrent + ", \"size\" : " + irowcount + ",\n"
                    + "  \"query\": {\n"
                    + "    \"bool\":  {\"should\": [\n"
                    + "          { \"match\":{\"text\":\"" + searchFrase + "\"}}\n"
                    + "      ]\n"
                    + "    }\n"
                    + "  }\n"
                    + "}";
            String url = "http://localhost:9200/" + scollection + "/doc/_search";
            HttpPost post = new HttpPost(url);
            StringEntity postingString = new StringEntity(query, "UTF-8");
            post.setEntity(postingString);
            post.setHeader("Content-type", "application/json");
            HttpResponse response1 = httpClient.execute(post);
            HttpEntity entity = response1.getEntity();
            String body = EntityUtils.toString(entity, "UTF-8");
            JsonObject jobj = new JsonParser().parse(body).getAsJsonObject();
            JsonObject jh = jobj.get("hits").getAsJsonObject();
            JsonElement tel = jh.get("total");
            int tot = 0;
            if (tel.isJsonObject()) {
                tot = tel.getAsJsonObject().get("value").getAsInt();
            } else {
                tot = tel.getAsInt();
            }
            if (tot > 0) {
                JsonArray ja = jh.get("hits").getAsJsonArray();
                int tam = ja.size();
                for (int i = 0; i < tam; i++) {
                    JsonObject jo = ja.get(i).getAsJsonObject();
                    if (jo != null) {
                        id = jo.get("_id").getAsString();
                        score = jo.get("_score").toString();
                        float newscore = Float.parseFloat(score);
                        if (newscore > 25) {
                            ids.add(id);
                            //System.out.println(cont + " la palabra " + word + " esta contenida en el id " + id + " con un score de " + score);
                            cont++;
                        }
                    }
                }
            }
        }
        return ids;
    }

    //function that checks if it has id
    //the function of this method is simple, it is only quality if an html with has a searched id.
    public static boolean hasId(Document doc, String id) {
        boolean error = false;
        try {
            String message = doc.getElementById(id).text();
            if (message.length() > 0) {
                error = true;
            }
        } catch (Exception e) {
        }

        return error;
    }

    //function to convert json to table
    private static boolean checkIfEmptyRow(List<String> rowData) {
        for (String td : rowData) {
            if (!td.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    //function to convert json to table
    private static List<String> getTableData(String str) {
        List<String> data = new ArrayList<String>();
        boolean inquote = false;
        StringBuilder buffer = new StringBuilder();
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '"') {
                inquote = !inquote;
                continue;
            }

            if (chars[i] == ',') {
                if (inquote) {
                    buffer.append(chars[i]);
                } else {
                    data.add(buffer.toString());
                    buffer.delete(0, buffer.length());
                }
            } else {
                buffer.append(chars[i]);
            }

        }
        data.add(buffer.toString().trim());
        return data;
    }

    //function to convert json to table
    private static String handleNewLine(String str) {
        StringBuilder buffer = new StringBuilder();
        char[] chars = str.toCharArray();
        boolean inquote = false;
        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == '"') {
                inquote = !inquote;
            }
            if (chars[i] == '\n' && inquote) {
                buffer.append("<br>");
            } else {
                buffer.append(chars[i]);
            }
        }
        return buffer.toString();
    }

    //function to convert json to table
    public static String csvToHtmlTable(String str, String name) {
        str = handleNewLine(str);
        String[] lines = str.split("\n");

        StringBuilder result = new StringBuilder();
        int cont = 0;
        for (int i = 0; i < lines.length; i++) {

            List<String> rowData = getTableData(lines[i]);

            if (checkIfEmptyRow(rowData)) {
                continue;
            }

            if (cont == 0) {
                result.append("<thead><tr>");
            } else {
                result.append("<tbody><tr>");
            }
            for (String td : rowData) {
                if (cont == 0) {
                    result.append(String.format("<th>%s</th>\n", td));
                } else {
                    result.append(String.format("<td>%s</td>\n", td));
                }

            }
            if (cont == 0) {
                result.append("</tr></thead>");
            } else {
                result.append("</tr></tbody>");
            }
            cont = 1;
        }
        conttable++;
        //here you add styles to download in pdf format so you can feel it
        return String.format("<p style=\"font-weight: bold\">" + name + "</p><table id=\"tabledowload" + conttable + "\" class=\"table table-bordered\">\n%s</table>", result.toString());
    }

    //function to extract text in elasticsearch
    //Extract the json document from elasticsearch
    public static String Extacjson(String id) throws IOException {
        String info = "";
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("http://localhost:9200/textnews/doc/" + id)
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();
        JsonObject jobj = new JsonParser().parse(response.body().string()).getAsJsonObject();
        JsonObject sour = jobj.get("_source").getAsJsonObject();
        info = sour.toString();
        return info;
    }

    //function to convert json to table
    //here send the json to a page and it returns the table transformed to html
    public static String JsontoCSV(String JSON, String name) throws IOException {
        String certificatesTrustStorePath = "<JAVA HOME>/jre/lib/security/cacerts";
        System.setProperty("javax.net.ssl.trustStore", certificatesTrustStorePath);
        OkHttpClient client1 = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType1 = MediaType.parse("text/plain");
        RequestBody body1 = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("json", JSON)
                .build();
        Request request1 = new Request.Builder()
                .url("https://json-csv.com/conversion/start")
                .method("POST", body1)
                .addHeader("Cookie", "ASP.NET_SessionId=1po1ndbtqn5apwhqe0a1ji0a; token=6ed237f9-af6b-4202-997e-9e169ca322a3")
                .build();
        Response response1 = client1.newCall(request1).execute();
        String JSON1 = response1.body().string();

        JSONObject obj = new JSONObject(JSON1);
        String href = obj.get("href").toString();
        URL url = new URL("https://json-csv.com" + href);

        //makes an exception to the certificate of the pages
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }

                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
        }

        URLConnection urlCon = url.openConnection();
        urlCon.connect();

        InputStream is = urlCon.getInputStream();

        File f = new File("sources_excel");
        String directorio = f.getAbsolutePath();
        String downloadsPath = f.getAbsolutePath();

        FileOutputStream fos = new FileOutputStream(directorio + ".txt");

        byte[] array = new byte[1000];
        int leido = is.read(array);
        while (leido > 0) {
            fos.write(array, 0, leido);
            leido = is.read(array);
        }

        is.close();
        fos.close();
        String cadena;
        String content = "";
        FileReader file = new FileReader(directorio + ".txt");
        BufferedReader b = new BufferedReader(file);
        while ((cadena = b.readLine()) != null) {
            System.out.println(cadena);
            content += cadena + "\n";
        }
        b.close();
        String tablehtml = csvToHtmlTable(content, name);
        FileUtils.deleteQuietly(new File(directorio + ".csv"));
        return tablehtml;
    }

}
