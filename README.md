# portal CPONet

Simple portal to access the CPONet


## Configuration
Before you start working you needed:
- git client
- maven 
- jdk 1.8
- A Java IDE of your choice (not mandatory)
- InstallCert (https://github.com/escline/InstallCert.git)

## Install certificate *
_* in some cases it is necessary to install certificates_
```
md d:\InstallCert
cd d:\InstallCert
git clone https://github.com/escline/InstallCert.git
cd InstallCert

javac InstallCert.java
java InstallCert sanciones.cnbv.gob.mx:443
keytool -exportcert -alias  sanciones.cnbv.gob.mx-1 -keystore jssecacerts -storepass changeit -file 1.cer
sudo keytool -importcert -alias sanciones.cnbv.gob.mx -keystore /Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home/jre/lib/security/cacerts -storepass changeit -file 1.cer

javac InstallCert.java
java InstallCert json-csv.com:443
keytool -exportcert -alias  json-csv.com-1 -keystore jssecacerts -storepass changeit -file 2.cer
sudo keytool -importcert -alias json-csv.com -keystore /Library/Java/JavaVirtualMachines/jdk1.8.0_241.jdk/Contents/Home/jre/lib/security/cacerts -storepass changeit -file 2.cer
```


**this steps are for Mac OS**

## Setup
In order to get the code:

- Create a folder (Windows): 
```
md d:\portalcponet
cd d:\portalcponet
git clone https://gitlab.com/luisgosmx/portalcponet.git
cd portalcponet
```
- Compile everything
```
mvn clean install
```
- Run the portal

```
java -jar jetty-runner.jar target\portalcponet-1.0.war
http://localhost:8080
```
- _In some cases it is necessary to copy and paste the file `chromedriver` in the following path `../GlassFish_Server/glassfish/domains/domain1/config/chromedriver`_

## Steps to deploy to the server*

_* This step is only necessary for the administrator_

```
log in to the server "delicias"
cd /cponet/portal
sudo deployportal.sh
wait 1 minute
ready
```



## List of available companies and people
**_Sanciones Consolidadas Mexico https://sanciones.cnbv.gob.mx_**  
**_Sanciones(mexico)_**

- LEON CABRERA FLORES
- JAIME ANTONIO GONZÁLEZ REMIS
- ANTONIO SALINAS VELASCO
- RAMÓN ARTURO GARCÍA CHÁVEZ
- SERGIO VARGAS VARGAS
- HUMBERTO GARZA VALDEZ
- JORGE TAPIA DEL BARRIO
- Fernando José Lezama Shiraishi
- Lorenzo Justiano Osorio Gómez
- Gustavo Madero Marcos
- Doris Emilia Magaña Canales
- José Luis Revilla Olavarrieta
- Ignacio Alejandro Llantada y Zapiain
- Lucia Sandoval Jimenez
- Humberto Alejandro Riquelme  Alcocer
- Mario Alberto Prieto Garza
- Rodolfo Alberto Tejada Gómez
- Oscar Gerardo de la Torre Martínez
- OSCAR FABIÁN SEBASTIÁN URIBE DE LA SIERRA
- Alonso Quintana Kawage
- CAJA POPULAR COMONFORT, S.C. DE A.P. DE R.L. DE C.V.
- Tarjetas Banamex, S.A. de C.V. Sociedad Financiera de Objeto Múltiple, Entidad Regulada
- CENTRO CAMBIARIO REGINA, S.A. DE C.V.
- MONEX OPERADORA DE FONDOS, S.A. DE C.V., MONEX GRUPO FINANCIERO
- SURA INVESTMENT MANAGEMENT MÉXICO, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN
- CENTRO CAMBIARIO GRUPO BARIVSA, S.A. DE C.V.
- ACRECENTA, S.C. DE A.P. DE R.L. DE C.V.
- OPERADORA ACTINVER, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, GRUPO FINANCIERO ACTINVER
- Operadora GBM, S.A. de C.V. Sociedad Operadora de Sociedades de Inversión
- BANCO SANTANDER (MEXICO), S.A., INSTITUCION DE BANCA MULTIPLE, GRUPO FINANCIERO SANTANDER MEXICO
- FEDERACION ATLANTICO PACIFICO DEL SECTOR DE AHORRO Y CREDITO POPULAR, A.C.
- CREDICIA, S.A. DE C.V., SOFOM, ENR., (ANTES CIA. ARRENDADORA, MEDIACIÓN Y ASESORÍA CREDITICIA, S.A. DE C.V., SOFOM, ENR.)
- KU-BO FINANCIERO, S.A. DE C.V., S.F.P.
- GRAMERCY MÉXICO NPL II, S.A. DE C.V., SOFOM E.N.R.
- CAJA POPULAR SAN BERNARDINO DE SIENA VALLADOLID, S.C. DE A.P. DE R.L. DE C.V.
- BBVA BANCOMER GESTIÓN, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, GRUPO FINANCIERO BBVA BANCOMER
- Instituto del Fondo Nacional para el Consumo de los Trabajadores
- OPERADORA GBM, S.A. de C.V., Sociedad Operadora de Sociedades de Inversión
- INTERACCIONES CASA DE BOLSA, S.A. DE C.V., GRUPO FINANCIERO INTERACCIONES
- INTERACCIONES SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, S.A. DE C.V., GRUPO FINANCIERO INTERACCIONES
- HSBC GLOBAL ASSET MANAGEMENT (MÉXICO), S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, GRUPO FINANCIERO HSBC
- CI FONDOS, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN
- OPERADORA INBURSA DE FONDOS DE INVERSIÓN, S.A. DE C.V., GRUPO FINANCIERO INBURSA
- COMPASS INVESTMENTS DE MEXICO, S.A. DE C.V.
- IMPULSORA DE FONDOS BANAMEX, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, INTEGRANTE DEL GRUPO FINANCIERO BANAMEX
- OPERADORA MIFEL, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, GRUPO FINANCIERO MIFEL
- VECTOR FONDOS, S.A. DE C.V., SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN
- SCOTIA FONDOS, S.A. DE C.V.,SOCIEDAD OPERADORA DE FONDOS DE INVERSIÓN, GRUPO FINANCIERO SCOTIABANK INVERLAT
- VALUE OPERADORA DE FONDOS DE INVERSIÓN, S.A. DE C.V., VALUE GRUPO FINANCIERO
- Old Mutual Operadora de Fondos, S.A. de C.V.

_for more elements see the following link_ (https://docs.google.com/spreadsheets/d/14i7sMJR31ngiZVUqlaMMIC8pH9KQ1MLjI8FkliQtHDU/edit?usp=sharing)

**_Office of Foreign Assets Control https://sanctionssearch.ofac.treas.gov_**  
**_sanciones (Brazil y Dominicana)_**
- INMOBILIARIA FER CADENA
- OLIVAS OJEDA, Marco Antonio
- SIN-MEX IMPORTADORA, S.A. DE C.V.	
- JACOME DEL VALLE, Omar Alfredo	
- BERMUDEZ SUAZA, Pedro Antonio	
- CONSULTORIA EN CAMBIOS FALCON S.A. DE C.V.	
- AERONAUTICA CONDOR S.A. DE C.V.	
- GRUPO GUADALEST S.A. DE C.V.	
- DE ICAZA LOZANO, Alejandro	
- ILC EXPORTACIONES, S. DE R.L. DE C.V.	
- MENDEZ VARGAS, Jose de Jesus	 	 
- RODRIGUEZ FERNANDEZ, Andre	
- TORO DIAZ, Diana Lorena	 
- URREA LENIS, Jair Fernando	
- GONZALEZ MEDINA, Jaime Andres		 
- COBO LEDESMA, Juan Carlos	
- GARCIA SANCHEZ, Ricardo	
- FLORES CACHO, Alejandro	 	
- GRUPO CRISTAL CORONA S.A. DE C.V.	
- MANTENIMIENTO, AERONAUTICA, TRANSPORTE, Y SERVICIOS AEREOS S.A. DE C.V.	
- CIFUENTES VILLA, Jorge Milton	
- GALLEGO MARIN, Fabian Rodrigo	
- CIFUENTES VILLA, Teresa de Jesus
- RED MUNDIAL INMOBILIARIA, S.A. DE C.V.
- INTERNATIONAL GROUP OIRALIH, S.A. DE C.V.
- GONZALEZ CARDENAS, Jorge Guillermo
- AMARAL AREVALO, Wendy Dalaithy
- LAREDO DONJUAN, Ruben


**_companies office of Jamaica https://www.orcjamaica.com/CompanySearchResults.aspx_**  
**_orcjamaica (Jamaica)_**

- GIESECKE Y DEVRIENT CURRENCY TECHNOLOGY DE MEXICO, S.A. DE C.V.
- GIESECKE Y DEVRIENT DE MEXICO , S.A. DE C.V.
- LITTLE MEXICO
- Intcomex Jamaica Limited
- ALL EYES H. Q.
- C & G GROCERY
- C & S TECHNOLOGY CONSTRUCTION SERVICES
- C AND G'S RESTAURANT
- C & A Variety Store
- C & A WINDOWS AND DOORS


**_INDECOPI https://servicio.indecopi.gob.pe/appCPCBuscador/_**  
**_indecopi (Peru)_**

- CMAC TACNA S.A.
- CMAC PIURA S.A.C. 
- CMAC TACNA S.A. 
- COMEXA 
- CMAC PAITA S.A. 
- BURGER KING - PIZZA HUT 
- VENTA DE VEHICULOS AUTOMOTORES. 
- FABRICACION DE PARTES Y PIEZAS DE CARPINTERIA PARA EDIFICIOS Y CONSTRUCCIONES 
- NOTARIA DEL POZO VALDEZ 
- CARITAS FELICES DE PAOGUI SRL 
- BANCO DE CREDITO DEL PERU 
- BANCO FALABELLA
- BANCO RIPLEY 




